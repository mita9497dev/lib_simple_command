#ifndef _SIMPLE_COMMAND_H__
#define _SIMPLE_COMMAND_H__

#include <Arduino.h>
#ifndef ESP32
#include <SoftwareSerial.h>
#endif

#define     _DEBUG__
#ifdef      _DEBUG__
    #define     debug               Serial
    #define     DB_Print(...)       debug.print(__VA_ARGS__);
    #define     DB_Println(...)     debug.println(__VA_ARGS__);
#else
    #define     debug               Serial
    #define     DB_Print(...) 
    #define     DB_println(...)
#endif

#define BUFFER_MAX_LEN 256

enum UART_TYPE 
{
    HARDWARE,
    SOFTWARE
};

class SimpleCommand
{
    public:
        SimpleCommand       () {};
        SimpleCommand       (HardwareSerial* port, uint32_t _baudrate);
        #ifndef ESP32
        SimpleCommand       (SoftwareSerial* port, uint32_t _baudrate);
        #endif
        
        void        begin               ();
        void        listen              ();
        char        read                ();
        bool        available           ();
      
        void        sendToPort          (const char* msg);
        bool        waitResponse        (uint32_t timeout);
        bool        readResponse        (uint32_t timeout, char* response, uint8_t rlen);
        
        void        setEndCharacter     (char character);
        void        setBaudrate         (uint32_t baudrate);
        void        setCorrectMessage   (bool correctMsg);
        bool        correctMessage      ();
        void        clearBuffer         ();

        void        DB_Buffer           ();

        bool        VARS_Read           (const char* pattern, ...);
        bool        hasOwnString        (const char* str);

    private:
        uint8_t         _bufferLen              = 0;
        char            _buffer[BUFFER_MAX_LEN] = "\0";
        char            _endCharacter           = '$';
        
        UART_TYPE       _uartType;          
        Stream*         _port;
        uint32_t        _baudrate           = 115200;

        uint32_t        _timeout            = 1000;
        uint32_t        _timeSend           = 0;
        uint32_t        _lastTimeRead       = 0;
        boolean         _correctMessage     = false;
};

#endif
