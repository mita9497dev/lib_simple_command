#include "simple_command.h"

SimpleCommand sCommand(&Serial, 115200);

const char* patternARD = "{%s;%s}";
const char* patternESP_PH = "ph:%f";
char address[10], command[20];
float ph;

void setup()
{
    sCommand.begin();
    delay(1000);
    // ESP CODE
//    sCommand.sendToPort("{0x3c;cal,10}");
}

void loop()
{
    sCommand.listen();

    // ARDUINO CODE
    if(sCommand.correctMessage())
    {
        sCommand.DB_Buffer();
        if(sCommand.VARS_Read(patternARD, &address, &command))
        {
            Serial.print("address: ");
            Serial.println(address);
            Serial.print("command: ");
            Serial.println(command);
            sCommand.sendToPort("ph:7");
        }
        sCommand.clearBuffer();
    }

    // ESP CODE
//    if(sCommand.correctMessage())
//    {
//        sCommand.DB_Buffer();
//        if(sCommand.VARS_Read(patternESP_PH, &ph))
//        {
//            Serial.print("phVal: ");
//            Serial.println(ph);
//        }
//
//        sCommand.clearBuffer();
//    }
}
