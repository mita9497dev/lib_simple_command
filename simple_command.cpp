#include "simple_command.h"

SimpleCommand::SimpleCommand(HardwareSerial* port, uint32_t baudrate)
{
    _uartType = HARDWARE;
    _baudrate = baudrate;
    _port = port;
}

#ifndef ESP32 
SimpleCommand::SimpleCommand(SoftwareSerial* port, uint32_t baudrate)
{
    _uartType = SOFTWARE;
    _baudrate = baudrate;
    _port = port;
}
#endif

void SimpleCommand::begin()
{
    if(_uartType == SOFTWARE)
    {
        #ifndef ESP32
        SoftwareSerial* p = (SoftwareSerial*)_port;
        p->begin(_baudrate);
        #endif
    }
    else 
    {
        DB_Println(_baudrate);
        HardwareSerial* p = (HardwareSerial*)_port;
        p->begin(_baudrate);
    }
    DB_Println(F("Set baudrate success"));
}

void SimpleCommand::setBaudrate(uint32_t baudrate)
{
    _baudrate = baudrate;
}

void SimpleCommand::setCorrectMessage(bool correctMessage)
{
    _correctMessage = correctMessage;
}

bool SimpleCommand::correctMessage()
{
    return _correctMessage;
}

void SimpleCommand::clearBuffer()
{
    _buffer[0] = '\0';
    _bufferLen = 0;
    setCorrectMessage(false);
}

char SimpleCommand::read()
{
    return _port->read();
}

bool SimpleCommand::available()
{
    return _port->available();
}

void SimpleCommand::sendToPort(const char* message)
{
    clearBuffer();
    
    _port->print(message);
    _port->println(_endCharacter);
    _timeSend = millis();
}

bool SimpleCommand::waitResponse(uint32_t timeout)
{
    while(timeout > 0) 
    {
        if(_port->available())
        {
            return true;
        }
        
        --timeout;
        delay(1);
    }
    return false;
}

bool SimpleCommand::readResponse(uint32_t timeout, char* response, uint8_t rlen)
{
    uint8_t len = 0;
    
    while(timeout > 0)
    {
        if(_port->available())
        {
            if(len >= rlen)
            {
                char c = _port->read();
                DB_Print(c);
                continue;
            }
            
            if (len == rlen - 2)
            {
                DB_Println(F("Response full of size"));
            }

            *response++ = _port->read();
            ++len;
        }

        --timeout;
        delay(1);
    }
    *response = '\0';
    DB_Println("");

    return len > 0;
}

void SimpleCommand::listen()
{
    uint32_t t = millis();

    while(_port->available())
    {
        char c = _port->read();
        DB_Print(c);
        
        if(_bufferLen < BUFFER_MAX_LEN - 1)
        {
            if (c == _endCharacter) 
            {
                DB_Println(F("\nCorrect Message"));
                setCorrectMessage(true);
                continue;
            }
            
            _buffer[_bufferLen++] = c;
            _buffer[_bufferLen] = '\0';
        }
        delay(1);
    }

    if(!correctMessage())
    {
        clearBuffer();
    }
}

void SimpleCommand::DB_Buffer()
{
    DB_Println("--BUFFER--");
    DB_Println(_buffer);
    DB_Println("--BUFFER--");
}

bool SimpleCommand::hasOwnString(const char* str)
{
    return strstr(_buffer, str) != nullptr;
}

bool SimpleCommand::VARS_Read(const char* pattern, ...)
{
    va_list ap;
    va_start(ap, pattern);

    const char* data = _buffer;

    while(*pattern && *data) 
    {
        while((*pattern && *data) && (*pattern == *data || *pattern == '*')) 
        { 
            pattern++; 
            data++; 
        }

        if (*pattern == '\0' || *data == '\0') 
        {
            va_end(ap);
            return *pattern == '\0';
        }
        
        if (*pattern++ != '%') 
        {
          va_end(ap);
          return false;
        }
        if (*pattern == 'f') 
        {
            pattern++;
            float* v = va_arg(ap, float*);
            
            char tmp[10];
            int i = 0;
            
            while (*data != '\0' && *data != *pattern) 
            {
                if (i >= 9) break;
                
                tmp[i++] = *data;
                data++;
            }
            tmp[i] = '\0';
            
            *v = atof(tmp);
        } 
        else if (*pattern == 'd') 
        {
            pattern++;
            int* v = va_arg(ap, int*);
            
            char tmp[10];
            int i = 0;
            
            while (*data != '\0' && *data != *pattern) 
            {
                if (i >= 9) break;
                
                tmp[i++] = *data;
                data++;
            }
            tmp[i] = '\0';
            
            *v = atoi(tmp);
        } 
        else if (*pattern == 's') 
        {
            pattern++;
            char* v = va_arg(ap, char*);
            char* tmp = v;
            int i = 0;
            
            while (*data != '\0' && *data != *pattern) 
            {
                if (i++ >= 127) break;
                *tmp++ = *data++;
            }
            *tmp = '\0';
        }
    }
    va_end(ap);
    
    return *pattern == '\0';
}
